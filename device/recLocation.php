<?php

	require '../keyclasses.php';
	$KC = new keyclass();
	$KC_D = new keyclass_devices();
	
	/*
		This php script is about collecting the location information from the devices and then inserting it in the database.
		All the information is received by "GET" requests. We will try and change this to POST requests.
	*/
	
	/*
		Attributes to receive:
			- "location_time_device"
			- "location_speed"
			- "location_lat"
			- "location_long"
			- "device_serial"
			- "device_mac"
	*/
	
	/*// GETs the information and sanitises the string.
	$location_time_device = $_GET["time_device"];
	$location_speed = $KC->sanitiseString($_GET["speed"]);
	$location_lat = $KC->sanitiseString($_GET["lat"]);
	$location_long = $KC->sanitiseString($_GET["long"]);
	$device_serial = $KC->sanitiseString($_GET["serial"]);
	$device_mac = $KC->sanitiseString($_GET["mac"]);*/
	
	// POSTs the information and sanitises the string.
	$location_time_device = $_POST["time_device"];
	$location_speed = $KC->sanitiseString($_POST["speed"]);
	$location_lat = $KC->sanitiseString($_POST["lat"]);
	$location_long = $KC->sanitiseString($_POST["long"]);
	
	$device_serial = $KC->sanitiseString($_POST["serial"]);
	//$device_serial = "AAAA";
	
	$device_mac = $KC->sanitiseString($_POST["mac"]);
	
	
		// Appending to log
	$writeLogLine = "\nLog: $location_time_device, $location_speed, $location_lat, $location_long, $device_serial, $device_mac" . PHP_EOL;
	
	$filePath = "./device.log";
	file_put_contents($filePath, $writeLogLine, FILE_APPEND | LOCK_EX);
	
	
	
	$answer = $KC_D->receiveLocation($location_time_device, $location_speed, $location_lat, $location_long, $device_serial, $device_mac);


	echo $answer;
?>