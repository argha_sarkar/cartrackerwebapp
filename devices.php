<?php 
	session_start(); 
	error_reporting(0);
	
	require 'keyclasses.php';
	
	if (isset($_SESSION["user_id"])) {
		$user_id = $_SESSION["user_id"];
		$name = $_SESSION["name"];
		$email = $_SESSION["email"];
	} else {
		header("Location: ./");
	}
?>


<!DOCTYPE html>
<head>
	<title>Find My Ride - Manage devices</title>
</head>
<body>

<!-- Php scripts for including other pages --> 

	<?php include 'header.php'; ?>
	
	<?php include 'devicesdis.php'; ?>
	
<!-- End of php scripts -->


	
</body>
</html>