<?php 
	session_start(); 
?>

<!DOCTYPE html>

<head>
  <title>Find My Ride - Share</title>
</head>

<body>

	<!-- FOR FACEBOOK SHARE BUTTON -->
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
			js = d.createElement(s); js.id = id;
			js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.0";
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
	</script>
	
	<?php include "header.php"; ?>
	
	<p class = "generalText">	
		This is the start of the project. If you could please help by sharing this project, I will be greatful! Thank you!
		<br><br>
		<div class="fb-share-button" data-href="https://findmyride.co.uk/project/" data-layout="button_count" style="padding-left:350px;"></div>
	</p>	
	
</body>

</html>
