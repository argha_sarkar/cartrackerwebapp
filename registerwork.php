<?php
	session_start();
	/*
		Importing database and keyclasses PHP file.
		Keyclasses has already imported the database file.
	*/
	require 'keyclasses.php';
	
	// Declaring the key class. It contains useful functions.
	$KC = new keyclass();
	//Declaring the key class for log in.
	$KC_L = new keyclass_login();
	// Declaring the database class.
	$DB = new Database;
	
	/*
		This bit is about getting all the information via POST 
	*/
	
	$fname = $_POST["fname"];
	//echo "$fname<br>";
	$sname = $_POST["sname"];
	//echo "$sname<br>";
	$email = $_POST["email"];
	//echo "$email<br>";
	$address = $_POST["address"];
	$postcode = $_POST["postcode"];
	$password = $_POST["password"];
	$rpassword = $_POST["rpassword"];
	
	/*
		All the user inputs now needs to be sanitised.
		They are also checked if they are generally valid.
		If any of them are invalid, an input error is raised.
	*/
	
	// Input error declaration
	$input_error = "";
	
	if ($KC->sanitiseString($fname) == "") {
		$input_error = "Please check first name. <br>";
	}
	
	if ($KC->sanitiseString($sname) == "") {
		$input_error = $input_error . "Please check surname. <br>";
	}
	
	if ($KC->sanitiseString($email) == "") {
		$input_error .= "Please check email. <br>";
	}
	
	if ($KC->sanitiseString($address) == "") {
		$input_error .= "Please check address. <br>";
	}
	
	if ($KC->sanitiseString($postcode) == "") {
		$input_error .= "Please check postcode. <br>";
	}
	
	if ($KC->sanitiseString($password) == "") {
		$input_error .= "Please check password. <br>";
	}
		
	if (strlen($password) < 2) {
		$input_error .= "Password is too short. <br>";
	}
	
	if ($KC->sanitiseString($rpassword) == "") {
		$input_error .= "Please check repeated password. <br>";
	}
	
	if ($password != $rpassword) {
		$input_error .= "Passwords do not match<br>";
	}
	
	if ($KC->checkEmailInDb($email) != false) {
		$input_error .= "Email already exists in database. <br>";
	}
	
	/* 
		Given above are all input checks to make sure all user inputs are fine.
		Given below is the code to insert it into the database.
		If everything is fine then the session variables are set. 
		The user is able to log in.
	*/
	
	if ($input_error != "") {
		header("Location: ./register.php?errors=$input_error");
	} else {
		// Inserting data into the database and the session variables are set here. 
		$KC->insertUserInDbP($fname, $sname, $email, $address, $postcode, $password);
		$_SESSION["user_id"] = $KC_L->getIdFromEmail($email);
		$_SESSION["email"] = $email;
		$_SESSION["name"] = $fname . " " . $sname;
		header("Location: ./");
	}
		
?>