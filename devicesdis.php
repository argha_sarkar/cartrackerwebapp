 <!-- Shows the devices owned by the user and gives the option of removing any from the list. -->

<!-- <p class="devicemanagement">Device Management</p> -->
<br><br><br>

<div id = "devicesmain">
	<form action="removeDevice.php" method="post">
		<!-- Displays the devices data in a table -->
 	
		<table class="devices">
 			<tr class="devices">
 				<th class="devices">Serial Number</th>
				<th class="devices">MAC Address</th>
				<th class="devices">Device name</th>
 				<th class="devices">Keep / Remove</th>
  			</tr>
  			
  		<?php
  			$KC_d = new keyclass_devices();
  			$result = $KC_d->getDevices($user_id);
  			
  			$devices_exist = false;
  			
  			if ($result->num_rows > 0) {
				// output data of each row
				while($row = $result->fetch_assoc()) {
					$device_serial = $row["device_serial"];
					$device_mac = $row["device_mac"];
					$device_name = $row["device_name"];
					echo "<tr>
							<td class='devices'>$device_serial</td>
							<td class='devices'>$device_mac</td>
							<td class='devices'><a href='location.php?serial=$device_serial&mac=$device_mac' style='text-decoration: none'>$device_name</a><br>
												<a href='locationHistory2.php?serial=$device_serial&mac=$device_mac&lcount=50' style='text-decoration: none'> View history</td>
							<td class='devices'><input type='radio' name='removeDevice' value='$device_serial'> </td>
						  </tr>";
				}
				$devices_exist = true;
				
			} else {
				echo "<tr>
						<td>No devices found</td>
					</tr>";
			}
			
  		?>
  			
 		</table>
 		
 		<div id="devicebuttonpadding">
 		<?php
 			
 			if ($devices_exist == true) {
 				echo "<br><input class='btn' type='submit' name='submit' value='Remove Device'>";
 			}
 			
 		?>
 		</div>
 
	</form>
</div>
 
 