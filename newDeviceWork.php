<?php
	session_start();
	if (!isset($_SESSION["user_id"])) {
		header("Location: ./");
	}
		
	require 'keyclasses.php';
	$KC = new keyclass();
	$KC_D = new keyclass_devices();
	
	// Contains all the error messages.
	$error_messages = "";
	
	$user_id = $_SESSION["user_id"];
	$email = $_SESSION["email"];
	$name = $_SESSION["name"];

	$device_serial = $_POST["serial"];
	$device_mac = $_POST["mac"];
	$device_name = $_POST["device_name"];
	
	$device_serial = $KC->sanitiseString($device_serial);
	$device_mac = $KC->sanitiseString($device_mac);
	$device_name = $KC->sanitiseString($device_name);
		
	echo "Serial: $device_serial<br>";
	echo "Mac: $device_mac<br>";
	
	/* Things to do 
    	- Check if the device exists in the database -> It must exist in database.
    	- Check if the device has an owner -> The device can't have an owner.
	*/
	
	// If the device exists, then the device_id is returned. Else -1.
	$device_id = $KC_D->deviceExists($device_serial, $device_mac);
	if ($device_id < 0) {
		// Device has not been registered on the network.
		$error_messages .= "Device has not been registered on the system.<br>";
		
	} else {
	
		// Device has been registered on the network. Need to check if it is already owned.
		$owner_user_id = $KC_D->deviceOwnStatus($device_id);
		
		if ($owner_user_id < 0) {
			// Device does not have a owner. The new device can be added.
			$KC_D->addDeviceOwner($user_id, $device_id);
			$KC_D->addDeviceName($device_id, $device_name);
		} else {
			// Devie already has an owner.
			$error_messages .= "Device is already owned. Please register a new device.<br>";
		}
		
 	}
	
	if ($error_messages != "") {
		header("Location: ./newDevice.php?errors=$error_messages");
	} else {
		header("Location: ./devices.php");
	}
?>