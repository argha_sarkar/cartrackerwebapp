<?php
	session_start();
	
	if (!isset($_SESSION["user_id"])) {
		header("Location: ./");
	}
	
	require 'keyclasses.php';
	$KC_D = new keyclass_devices();
	
	$user_id = $_SESSION["user_id"];
	
	if (isset($_POST["removeDevice"])) {
		$device_serial = $_POST["removeDevice"];
	
	
		$device_id = $KC_D->getDeviceIdBySerial($device_serial);
		$owner_status = $KC_D->verifyDeviceOwner($user_id, $device_id);
	
		if ($owner_status == true) {
			$KC_D->deleteDeviceUserRef($device_id);
			header("Location: ./devices.php");
		}
		
	} else {
		echo "Please select a device to delete!";
		header("refresh:5;url=./devices.php");
	}
?>