<?php
	require '../keyclasses.php';
	$KC = new keyclass();
	$KC_D = new keyclass_devices();
	$KC_T = new keyclass_test();
	
	// Number of records in the database being used
	$numRecs = $KC_T->testLatency1();
	// Importance of each record (weightage)
	$imp = 1 / $numRecs;
	
	echo "Number of records: $numRecs<br>";
	//echo "Weightage of each record: $imp<br>";
	
	// All time pairs in the database
	$timePairs = $KC_T->returnAllTimes();
	
	// Cumulative time difference
	$curTimeDiff = 0;

	//String containing all delays
	$delayString = "";
	
	// Highest delay
	$highestDelay = 0;
	$lowestDelay = 0;
	
	if ($timePairs->num_rows > 0) {
		
		while ($row1 = $timePairs->fetch_assoc()) {
			$deviceTime = $row1["location_time_device"];
			$serverTime = $row1["location_time_server"];
			
			// Calculates time differnece
			$time1 = strtotime($deviceTime);
			$time2 = strtotime($serverTime);
			
			$curDiff = ($time2 - $time1) + 2;
			$curTimeDiff += $curDiff;
			
			if ($curDiff < 0) {
				$curDiff = 0;
			}
			
			$delayString .= $curDiff . "<br>";
			
			if ($lowestDelay > $curDiff) {
				$lowestDelay = $curDiff;
			}
			
			if ($highestDelay < $curDiff) {
				$highestDelay = $curDiff;
			}
		}
	}	
	
	$averTimeDiff = $curTimeDiff / $numRecs;

	echo "Total time difference in seconds: $curTimeDiff<br>";
	echo "Average latency in seconds: $averTimeDiff<br>";
	echo "Highest delay: $highestDelay<br>";
	echo "Lowest delay: $lowestDelay<br>";
	
	echo "<br>";
	
	echo $delayString;
	
?>