<?php 
	/* Checks if the user is logged in or out. 
	   If the user is logged in, then gets the user ID of the user.
	*/
	session_start(); 
	$loggedIn = true;
	
	if (isset($_SESSION["user_id"]) == true) {
		$loggedIn=true;
		$user_id = $_SESSION["user_id"];
		$name = $_SESSION["name"];
		$email = $_SESSION["email"];
    } else {
		$loggedIn=false;
		header("Location: https://findmyride.co.uk/project");
    }
    // API KEY = AIzaSyBdc4m0AjMZJDUZXifsWhBLxcmK04uklXw	
    require 'keyclasses.php';
    $KC_L = new keyclass_locations();
	$KC_D = new keyclass_devices();
?>

<!DOCTYPE html>
<head>
	<title>Find My Ride</title>
	
	<style type="text/css">
      html, body, #map-canvas { height: 90%; margin: 0; padding: 10px;}
    </style>
    <script type="text/javascript"
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBdc4m0AjMZJDUZXifsWhBLxcmK04uklXw">
    </script>
     
	<?php
		// GETs the device information 
		$device_serial = $_GET["serial"];
		$device_mac = $_GET["mac"];
		//Number of records to be shown per polyline
		$recPerLine = $_GET["lcount"];
		
		// Last known location of the device
		$loc_long = $KC_L->getLastDeviceLocationLong($device_serial, $device_mac);
		$loc_lat = $KC_L->getLastDeviceLocationLat($device_serial, $device_mac);
		$server_time = $KC_L->getLastDeviceServerTime($device_serial, $device_mac);
		
		// This bit of the code is to verify that the person logged in actually owns the device
		// Sets a boolean indicating if the owner is valid or not
		$validOwner = false;
		// Gets the device id if it exists
		$device_id = $KC_D->deviceExists($device_serial, $device_mac);
		
		if ($device_id > -1) {
			$validOwner = $KC_D->verifyDeviceOwner($user_id, $device_id);
		}
		/*
			Need to create an array count. Each element of the array will be a polyline. (An array of arrays)
			Each polyline will be formed of coordinates stored in an element of the parent array.
		*/
		//Current polycount number. Will stop plotting polylines when this reaches $maxLine value
		$polyLineCount = 0;
		$maxLines = $KC_L->getLocationsCount($device_serial, $device_mac, $recPerLine);
				
		if ($validOwner) {
			echo "
			<script type='text/javascript'>
			
			function initialize() {
				var mapOptions = {
					// Last known location of the device
					center: { lat: $loc_lat, lng: $loc_long },
					zoom: 15
				};
				var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
			
				// To add the marker to the map, use the 'map' property
				var marker = new google.maps.Marker({
					position: { lat: $loc_lat, lng: $loc_long },
					map: map,
					icon: '/project/resources/images/marker.png',
					labelContent: 'hhhhhhhhhhh',
					labelAnchor: new google.maps.Point(22, 0),
					labelClass: 'labels', // the CSS class for the label
					labelStyle: {opacity: 0.75}

				});
			
				var iw1 = new google.maps.InfoWindow({
					content: 'Time: $server_time'			//content: 'Time'
				});
				
				// Will contain the location for every 100 records
				var markerStack = [];
				";
			
			// Converts from string to integer
			$maxLinesVal = intval($maxLines);
			//var_dump($maxLinesVal);
			//var_dump($recPerLine);
				
			while ($polyLineCount < $maxLinesVal) {
				$allLocations = $KC_L->getAllDeviceLocations($device_serial, $device_mac, ($polyLineCount * $recPerLine), $recPerLine);
				echo "
				var polyLine$polyLineCount = [";
				
				$locationRowCount = 0;
				$curLocationLat = 0.0;
				$curLocationLong = 0.0;
				$curLocSpeed = 0.0;
				if ($allLocations->num_rows > 0) {
					while ($rowLocations = $allLocations->fetch_assoc()) {
						$locationLat = $rowLocations['location_lat'];
						$locationLong = $rowLocations['location_long'];	
						$curLocSpeed = $rowLocations['location_speed'];
						echo "new google.maps.LatLng($locationLat, $locationLong),";
						if ($locationRowCount == ($recPerLine / 5)) {
							$curLocTime = $rowLocations['location_time_server'];	
							$curLocationLat = $locationLat;
							$curLocationLong = $locationLong;
						}
						$locationRowCount++;
					}
				}
				
				echo "
				];
				";
				
				
				echo "
				markerStack.push('$curLocTime');
				";
				
				if ($curLocSpeed > 1) {
				
				echo "
				var marker$polyLineCount = new google.maps.Marker({
					position: { lat: $curLocationLat, lng: $curLocationLong },
					map: map,
					labelContent: 'hhhhhhhhhhh',
					labelAnchor: new google.maps.Point(22, 0),
					labelClass: 'labels', // the CSS class for the label
					labelStyle: {opacity: 0.75}
				});
				
				var iw1$polyLineCount = new google.maps.InfoWindow({
					content: 'Time: $curLocTime'			//content: 'Time'
				});
				
				google.maps.event.addListener(marker$polyLineCount, 'click', function (e) { iw1$polyLineCount.open(map, this); });
				
				";
				}
				
				echo "
				var linePath$polyLineCount = new google.maps.Polyline({
					path: polyLine$polyLineCount,
					geodesic: true,
					strokeColor: '#FF0000',
					strokeOpacity: 1.0,
					strokeWeight: 2

				});
				linePath$polyLineCount.setMap(map);
				";
				
				$polyLineCount++;
			}
			
			echo "
				google.maps.event.addListener(marker, 'click', function (e) { iw1.open(map, this); });

			}
			google.maps.event.addDomListener(window, 'load', initialize);
			
			</script>";
		}
		
	?>
    
</head>
<body>

	<?php
		include 'header.php';
		//$rowCount = $KC_L->getLocationsCount($device_serial, $device_mac, 100);
		//echo "<br><br>$rowCount<br><br>";
		include 'locationDis.php';
	?>
</body>
</html>