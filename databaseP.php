<?php

class DatabaseP {

	/*
		Connections to the database and returns the connection as a variable.
		Contains database connection credentials. 
	*/
	function getConnection() {

		$servername = "localhost";
		$username = "root";
		$password = "pooface";
		$dbName = "argha_project";
		
		$conn = new mysqli($servername, $username, $password, $dbName);
		
		if (!$conn) {
			die("Connection failed: " . mysqli_connect_error());
			echo "NOT connected<br>";
		}
			
		return $conn;
	}

	/*
		Closes a database connection.
	*/
	function closeConnection($conn) {
		$conn->close();
	}
	
	/*
		Tries to prepare the sql statement.
	*/
	function prepareStmt($query) {
	
		// Gets the database connection.
		$db = $this->getConnection();
		
		/*
			Returns the prepared statement if it was successful.
			If the preparation failed, then a FALSE result is returned.
		*/
		if ($prepQuery = $db->prepare($query)) {
			// Success
			return $prepQuery;
		} else {
			// Failure
			return false;
		}
	
	}
		
	/*
		Executes a query in a DANGEROUS way. Vulnerable to SQL injection.
	*/
	function execV($query) {
		$db = $this->getConnection();
		$db->query($query);
		$db->close();
	}
	
	/*
		Executes a query in a DANGEROUS way and returns the results. Vulnerable to SQL injection.
	*/
	function execReturnV($query) {
		/*
		Use this for most select and other queries
		*/
		$db = $this->getConnection();
		
		$result = $db->query($query);
		//var_dump($db);
		$db->close();
	
		return $result;	
	}

	
}

?>