<?php 
	session_start(); 
	error_reporting(0);
	if (isset($_SESSION["user_id"])) {
		header("Location: ./");
	}
?>

<!DOCTYPE html>
<head>
  <title>Find My Ride - Login</title>
</head>

<body>

	<?php include 'header.php'; ?>
  
	<?php include 'logindis.php'; ?>

	<p class = "registererror">
		<?php
			$error_flag = $_GET["errors"];
			if ($error_flag != "false") {
				echo "<br>" . $error_flag;
			}
		?>
		
  	</p>
  	
</body>

</html> 